import com.epam.Malashnyak.JUNIT.IProc;
import com.epam.Malashnyak.JUNIT.IProc.*;
import com.epam.Malashnyak.JUNIT.SampleClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MockTest {

  @Mock private IProc dosmt;

  @InjectMocks private SampleClass sc = new SampleClass();

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void mockTest() {
    when(dosmt.get(1.0)).thenReturn(0.1);
    assertEquals(0.1, sc.get(1.0));
  }
}
