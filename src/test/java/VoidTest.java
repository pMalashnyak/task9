import com.epam.Malashnyak.JUNIT.SampleClass;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

public class VoidTest {
  @BeforeEach
  public void beforeTest() {
    System.out.println("Void test started");
  }

  @AfterEach
  public void afterTest() {
    System.out.println("Void test ended");
  }

  @Test(expected = IOException.class)
  public void noFileTest() throws IOException {
    SampleClass sc = new SampleClass("");
    sc.openFile();
  }
}
