import com.epam.Malashnyak.JUNIT.SampleClass;
import org.junit.Test;
import org.junit.jupiter.api.*;

import java.lang.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

public class SampleClassTests {

  @BeforeEach
  public void beforeTest() {
    System.out.println("Test started");
  }

  @AfterEach
  public void afterTest() {
    System.out.println("Test ended");
  }

  @Test
  public void testAdd() {
    SampleClass sample = new SampleClass(3, 5);
    int result = sample.add();
    assertEquals(result, 8, "Result: " + result + " != 8");
  }

  @Test
  public void testDivision1() {
    SampleClass sample = new SampleClass(4, 2);
    double result = sample.divide();
    assumeTrue(result == 2.0, "Result: " + result + " != 2");
  }

  @Test(expected = ArithmeticException.class)
  public void testDivision() {
    SampleClass sample = new SampleClass(4, 0);
    sample.divide();
  }
}
